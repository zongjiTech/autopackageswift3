
//
//  WebServerMain.swift
//  AutoPackageipa
//
//  Created by Lifefarmer on 2016/10/12.
//  Copyright © 2016年 danteng. All rights reserved.
//

import Foundation
import Vapor

class WebServer{
    
    let drop = Droplet()
    
    static var share:WebServer{
        return WebServer()
    }
    
    func startServer(){
        
        self.RegistRouter()
        
        drop.run()
    }
    
}
