//
//  ViewController.swift
//  AutoPackageipa
//
//  Created by Lifefarmer on 2016/10/12.
//  Copyright © 2016年 danteng. All rights reserved.
//

import Cocoa
import RxCocoa

class ViewController: NSViewController {

    @IBOutlet var textBack: NSScrollView!
    @IBOutlet var resultView: NSTextView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadSubViews()
        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    let commentInput = NSTextField()
    
    func loadSubViews(){
        textBack.layer?.cornerRadius = 6
        textBack.layer?.masksToBounds = true
        
        self.view.addSubview(commentInput)
        commentInput.snp.makeConstraints( { (make) in
            make.left.equalTo(view).offset(20)
            make.right.equalTo(view).offset(-128)
            make.bottom.equalTo(view).offset(-8)
            make.height.equalTo(35)
        })
        
        let startBut = NSButton()
        startBut.bezelStyle = .rounded
        startBut.title = "执行"
        _ = startBut.rx.tap.subscribe(onNext: { () in
            //RedisCenter.share.setValue(key: "mykey", value: "lifefarmer")
            RedisCenter.share.getValue(key: "mykey", com: { (value) in
                self.resultView.insertText(value, replacementRange: NSRange(0..<0))
                //print(value)
            })
                //FileCenter.share.readCertificate()
            //updateVersion(version: "1.1.1")
            //setPlist(name: "appstore", type: .appstore)
            }, onError: nil, onCompleted: nil, onDisposed: nil)
        self.view.addSubview(startBut)
        startBut.snp.makeConstraints( { (make) in
            make.right.equalTo(view).offset(-20)
            make.bottom.equalTo(view).offset(-8)
            make.height.equalTo(35)
            make.width.equalTo(100)
        })
    }
    
    

    func runScript(){
        RunScript.share.run("/Users/farmerwu_pc/Documents/水电管家用户", comment: self.commentInput.stringValue.components(separatedBy: ";")) { (output) in
            //print(output)
            self.resultView.string = output
            //self.resultView.insertText(output, replacementRange: NSRange(0..<0))
        }
    }
    
    

}

