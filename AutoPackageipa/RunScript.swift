//
//  RunScript.swift
//  AutoPackageipa
//
//  Created by Lifefarmer on 2016/10/12.
//  Copyright © 2016年 danteng. All rights reserved.
//

import Foundation

class RunScript{
    
    static var share:RunScript{
        return RunScript()
    }
    
    func run(_ path:String,comment:[String],com:((_ output:String)->Void)){
        let task = Process()
        
        // Set the task parameters
        task.launchPath = "/bin/sh"//"/usr/bin/env"
        task.currentDirectoryPath = path
        task.arguments = ["-c"] + comment
        
        // Create a Pipe and make the task
        // put all the output there
        let pipe = Pipe()
        task.standardOutput = pipe
        
        // Launch the task
        task.launch()
        
        // Get the data
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        com(output as! String )
    }
    
    
}
