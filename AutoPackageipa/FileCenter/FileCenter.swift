
//
//  FileCenter.swift
//  AutoPackageipa
//
//  Created by Lifefarmer on 2016/10/12.
//  Copyright © 2016年 danteng. All rights reserved.
//

import Foundation
import Security

enum ArchiveType:String {
    case appstore = "app-store"
    case development
    case adhoc = "ad-hoc"
    case enterprise
}
let teamID = "MA57VG5238"
class FileCenter {
    
    static var share : FileCenter{
        return FileCenter()
    }
    
    let manager = FileManager.default
    
    func setPlist(name:String,type:ArchiveType){
        let path = "/Users/farmerwu_pc/Downloads/\(name).plist"
        let plistfile = FileHandle(forWritingAtPath: path)
        if let  file = plistfile {
            let data = "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n<plist version=\"1.0\">\n<dict>\n<key>method</key> \n<string>\(type.rawValue)</string>\n<key>teamID</key>\n<string>\(teamID)</string>\n</dict>\n</plist>".data(using: String.Encoding.utf8)
            file.write(data!)
            
            
            file.closeFile()
        }else {
            manager.createFile(atPath: path, contents: nil, attributes: nil)
            setPlist(name: name,type: type)
        }
    }
    
    func updateVersion(version:String ){
        let rootPath = "/Users/farmerwu_pc/Documents/水电管家用户/WaterManager/Info.plist"
        if let plistInfo = NSMutableDictionary(contentsOfFile: rootPath){
            print(plistInfo)
            plistInfo["CFBundleShortVersionString"] = version
            
            
            plistInfo.write(toFile: rootPath, atomically: false)
        }
    }
    
    func readCertificate(){
        
        let pdata: NSData = try! NSData(contentsOfFile: "/Users/farmerwu_pc/Desktop/证书2.p12")
        let dictionary = [String(kSecImportExportPassphrase) : "123456"]
        var keyref: CFArray?
        //var error = SecPKCS12Import(pdata, dictionary as CFDictionary, keyref as! CFArray?)
        SecPKCS12Import(pdata, dictionary as CFDictionary, &keyref )
        
        //retrieve file entries
        let list = keyref as! NSArray
        print(list.firstObject )
    }
}
