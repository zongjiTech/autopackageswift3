//
//  RedisCenter.swift
//  AutoPackageipa
//
//  Created by Lifefarmer on 2016/10/13.
//  Copyright © 2016年 danteng. All rights reserved.
//

import Foundation


class RedisCenter{
    static var share:RedisCenter{
        return RedisCenter()
    }
    
    private let config = RedbirdConfig(address: "127.0.0.1", port: 6379, password: nil)
    private var  client : Redbird?{
        do{
            return try Redbird(config: config)
        }catch{
            Notice.waringMessage(message: "Can't Find Redis-Server @127.0.0.1")
            return nil
        }
    }
    
    
    func setValue(key:String,value:String){
        self.runComment("set", params: [key,value]) { (res, error) in
            
        }
    }
    
    func getValue(key:String,com:@escaping ((String)->Void)){
        self.runComment("get", params: [key]) { (res, error) in
            com(res)
        }
    }
    
    
    func runComment(_ name: String, params: [String] = [],com:((_ res:String,_ error:Error?)->Void)){
        
        do {
            if let RedisClient = client{
                let response = try RedisClient.command(name, params: params).toString()
                print("Redis \(name) sucess")
                com(response, nil)
            }
        } catch {
            com("", error)
        }
    }
    
    

}
