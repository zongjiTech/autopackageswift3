import PackageDescription

let package = Package(
    name: "WebServer",
    dependencies: [
        .Package(url: "https://github.com/vapor/vapor.git", versions: Version(1,1,0)..<Version(2,0,0)),
        .Package(url: "https://github.com/vapor/mysql-provider.git", majorVersion: 1, minor: 1)
//        .Package(url: "https://github.com/vapor/cmysql",majorVersion: 1)
//        .Package(url: "https://github.com/vapor/redis-provider.git", majorVersion: 0)
    ]
)
