
//
//  BaseDataTable.swift
//  AutoGetPrice
//
//  Created by Lifefarmer on 2016/10/21.
//  Copyright © 2016年 danteng. All rights reserved.
//

import Foundation
import RealmSwift

class ProductTypeTable:Object{
    dynamic var ID = String()
    dynamic var Name = String()//分类名称
    override static func primaryKey() -> String? {
        return "ID"
    }
}

class ProductCityTable:Object{
    dynamic var ID = String()
    dynamic var City = String()//分类名称
}

class CityTable:Object{
    dynamic var ID = String()
    dynamic var CityName = String()//城市名称
    override static func primaryKey() -> String? {
        return "ID"
    }
}
/*
 {
 "spec": "-",
 "isDelear": "0",
 "breed": "统一废铁",
 "price": "1250",
 "other": "",
 "raise": "+20",
 "mcodes": "13238统一废铁-上海",
 "place": "上海",
 "note": "市场不含税价"
 }
 */
class MarcketPrice:Object{
    dynamic var ID = String()
    dynamic var typeName = String()//分类名称
    dynamic var place = String()
    dynamic var price = String()
    dynamic var isGood = Bool()
    dynamic var deliveryStatus = String()//交货状态
    dynamic var standardName = String()//规格
    dynamic var note = String()//备注
    dynamic var time = String()
    override static func primaryKey() -> String? {
        return "ID"
    }
}
