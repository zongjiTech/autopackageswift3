//
//  APIPOST.swift
//  AutoGetPrice
//
//  Created by Lifefarmer on 2016/10/21.
//  Copyright © 2016年 danteng. All rights reserved.
//

import Foundation
import Alamofire

func getCityName(cityId:String)->String?{
    return realm.objects(CityTable.self).filter("ID == '\(cityId)'").first?.CityName
}
func getProductName(breedId:String)->String?{
    return realm.objects(ProductTypeTable.self).filter("ID == '\(breedId)'").first?.Name
}

class APIPOST {
    
    static func getBaseType(com:@escaping ((_ res:JSON)->Void)){
        let url = "http://mysteelapi.steelphone.com/v4/market/breed.htm"
        let parame = [
            "cellphone":"13601890712",
            "machineCodec":"41F0D0D9-6328-4E44-9759-F69F39654043",
            "ostype":"2",
            //"protocolVersion":"4.2.0",
            "userId":"380979"
        ]
        HttpPost(url, parame: parame) { (res) in
            com(res)
        }
    }
    
    static func updateCity(breedId:String,com:@escaping ((_ res:JSON)->Void)){
        let url = "http://mysteelapi.steelphone.com/v4/market/city.htm"
        let parame = [
            "breedId":breedId,
            "isNeedLog":"0",
            "cellphone":"13601890712",
            "machineCodec":"41F0D0D9-6328-4E44-9759-F69F39654043",
            "ostype":"2",
            //"protocolVersion":"4.2.0",
            "userId":"380979"
        ]
        HttpPost(url, parame: parame) { (res) in
            com(res)
        }
    }
    
    static func getDetilPrice(breedId:String,cityId:String,com:@escaping ((_ res:JSON)->Void)){
        let url = "http://mysteelapi.steelphone.com/v4/market/getMarket.htm"
        let parame = [
            "breedId":breedId,
            "breedName":getProductName(breedId: breedId)!,
            "cityId":cityId,
            "cityName":getCityName(cityId: cityId)!,
            "cellphone":"13601890712",
            "machineCodec":"41F0D0D9-6328-4E44-9759-F69F39654043",
            "ostype":"2",
            "isSub":"0",
            "marketId":"",
            "tableId":"",
            "type":"price",
            "deepBreedId":"",
            //"protocolVersion":"4.2.0",
            "userId":"380979"
        ]
        HttpPost(url, parame: parame) { (res) in
            com(res)
        }
    }
    
    static func clearHistory(com:@escaping ((_ res:JSON)->Void)){
        let url = "http://mysteelapi.steelphone.com/v4/market/breed/logs/clear.htm"
        let parame = [
            "cellphone":"13601890712",
            "machineCodec":"41F0D0D9-6328-4E44-9759-F69F39654043",
            "ostype":"2",
            "userId":"380979"
        ]
        HttpPost(url, parame: parame) { (res) in
            com(res)
        }
    }
    
    
    // MARK: - 初始接口
    //private static let domain = "https://www.ezhangyu.com/index.php/Appapi/Warm/"
    private static func HttpPost(_ url:String,parame:[String:String]? = nil,com:@escaping ((_ res:JSON)->Void)){
        
        Alamofire.request(url, method: .post, parameters: parame, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if let getDate = response.result.value as? NSDictionary{
                com(JSON(getDate))
            }else{
                com(JSON("{}"))
            }
        }

        
    }
}
