//
//  StoryBoard.swift
//  guijiaoweike
//
//  Created by Lifefarmer on 2016/9/27.
//  Copyright © 2016年 danteng. All rights reserved.
//

import Cocoa


protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
}

extension NSStoryboard{
    
    enum Storyboard : String {
        case Main
        case Login
    }

    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.rawValue, bundle: bundle)
    }
    
    func instantiate<T: NSViewController>() -> T where T: StoryboardIdentifiable {
        
        let optionalViewController = self.instantiateController(withIdentifier: T.storyboardIdentifier)
        guard let viewController = optionalViewController as? T  else {
            fatalError("Couldn’t instantiate view controller with identifier \(T.storyboardIdentifier)")
        }
        
        return viewController
    }
    
    var Name:String{
        return self.value(forKey: "name") as! String
    }
    
    var rootController:NSViewController{
        if let viewController = self.instantiateInitialController(){
            return viewController as! NSViewController
        }else{
            return self.instantiateController(withIdentifier: self.Name) as! NSViewController
        }
    }
}

extension StoryboardIdentifiable where Self: NSViewController {
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
}
extension NSViewController : StoryboardIdentifiable { }
