//
//  Float.swift
//  WaterWoker
//
//  Created by Lifefarmer on 16/6/25.
//  Copyright © 2016年 zongji. All rights reserved.
//

import Foundation

public extension Float{
    func toString(NumOfDecimal num:UInt8)->String{
       return  NSString(format: "%.\(num)f" as NSString, self) as String
    }
//    func toString()->String{
//        return  "\(self)"
//    }
}
