//
//  String.swift
//  ExSwift
//
//  Created by pNre on 03/06/14.
//  Copyright (c) 2014 pNre. All rights reserved.
//

import Cocoa

extension Character {
    func toInt() -> Int? {
        return String(self).toInt()
    }
}

public extension String {
    
    /**
     String range
     */
    subscript (r: Range<Int>) -> String {
        get {
            let startIndex = self.characters.index(self.startIndex, offsetBy: r.lowerBound)
            let endIndex   = self.characters.index(self.startIndex, offsetBy: r.upperBound)
            
            return self[Range(startIndex..<endIndex)]
        }
    }
    func lableHeight(_ fontOfSize:CGFloat,maxWidth:CGFloat) -> CGFloat {
        let Text = self as NSString
        let oneFrame = Text.size(withAttributes: [NSFontAttributeName:NSFont.systemFont(ofSize: fontOfSize)])
        return (oneFrame.width + maxWidth)/(maxWidth) * oneFrame.height
    }
    
    
    /**
     String Base64
     */
    var Base64Encoded: String {
        let utf8str = self.data(using: String.Encoding.utf8)
        return utf8str!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }
    var Base64Decoded: String {
        
        if let DaData = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0))
        {
            
            if let data = String(bytes: DaData.bytes, encoding: String.Encoding.utf8){
                return data
            }else{
                return ""
            }
        }else{
            return ""
        }
        
        
    }
    
    func isValidate(_ RegEx: String) -> Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", RegEx)
        return predicate.evaluate(with: self)
    }
    
    func isValidatePhoneNumber() -> Bool {
        return self.isValidate("^[1]{1}\\d{10}$")
    }
    
    func isValidatePassword() -> Bool {
        return (self.length <= 18 && self.length >= 6)
        //return self.validate("^[A-Z0-9a-z]{6,18}")
    }
    func isValidateNumber() -> Bool {
        return self.isValidate("^[0-9]{1}$")
    }
    
    func isValideteDecimal(_ NumofDecimal:UInt=2)->Bool{
        if self.isValidateNumber(){
            return true
        }
        return self.isValidate("^[0-9]+.[0-9]{0,\(NumofDecimal)}$")
    }
    
    func isValidIDCard() -> Bool {
        if self.length != 18 {
            return false
        }
        
        let mmdd = "(((0[13578]|1[02])(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)(0[1-9]|[12][0-9]|30))|(02(0[1-9]|[1][0-9]|2[0-8])))"
        let leapMmdd = "0229"
        let year = "(19|20)[0-9]{2}"
        let leapYear = "(19|20)(0[48]|[2468][048]|[13579][26])"
        let yearMmdd = year + mmdd
        let leapyearMmdd = leapYear + leapMmdd
        let yyyyMmdd = "((\(yearMmdd))|(\(leapyearMmdd))|(20000229))"
        let area = "(1[1-5]|2[1-3]|3[1-7]|4[1-6]|5[0-4]|6[1-5]|82|[7-9]1)[0-9]{4}"
        let regex = "\(area)\(yyyyMmdd)[0-9]{3}[0-9Xx]"
        
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        
        if predicate.evaluate(with: self) == false {
            return false
        }
        
        // Swift 2.0 pls change to Array(self.characters)
        let chars = Array(self.uppercased().characters)
        
        let summary: Int = (chars[0].toInt()! + chars[10].toInt()!) * 7
            + (chars[1].toInt()! + chars[11].toInt()!) * 9
            + (chars[2].toInt()! + chars[12].toInt()!) * 10
            + (chars[3].toInt()! + chars[13].toInt()!) * 5
            + (chars[4].toInt()! + chars[14].toInt()!) * 8
            + (chars[5].toInt()! + chars[15].toInt()!) * 4
            + (chars[6].toInt()! + chars[16].toInt()!) * 2
            + chars[7].toInt()!
            + chars[8].toInt()! * 6
            + chars[9].toInt()! * 3
        
        let remainder = summary % 11
        let checkString = "10X98765432"
        
        let checkBit = Array(checkString.characters)[remainder]
        
        return (checkBit == chars.last)
    }

    func toURL()->URL?{
        return URL(string: self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
    }

    
    
    /**
        String length
    */
    var length: Int { return self.characters.count }

    /**
        self.capitalizedString shorthand
    */
    var capitalized: String { return self.capitalized }


    func trimmedLeft (characterSet set: CharacterSet = CharacterSet.whitespacesAndNewlines) -> String {
        if let range = rangeOfCharacter(from: set.inverted) {
            return self[range.lowerBound..<endIndex]
        }

        return ""
    }

    @available(*, unavailable, message: "use 'trimmedLeft' instead") func ltrimmed (_ set: CharacterSet = CharacterSet.whitespacesAndNewlines) -> String {
        return trimmedLeft(characterSet: set)
    }

    /**
        Strips the specified characters from the end of self.

        - returns: Stripped string
    */
    func trimmedRight (characterSet set: CharacterSet = CharacterSet.whitespacesAndNewlines) -> String {
        if let range = rangeOfCharacter(from: set.inverted, options: NSString.CompareOptions.backwards) {
            return self[startIndex..<range.upperBound]
        }

        return ""
    }

    @available(*, unavailable, message: "use 'trimmedRight' instead") func rtrimmed (_ set: CharacterSet = CharacterSet.whitespacesAndNewlines) -> String {
        return trimmedRight(characterSet: set)
    }

    
    func trimmed () -> String {
        return trimmedLeft().trimmedRight()
    }


    func toDouble() -> Double? {

        let scanner = Scanner(string: self)
        var double: Double = 0

        if scanner.scanDouble(&double) {
            return double
        }

        return nil

    }

    /**
       Parses a string containing a float numerical value into an optional float if the string is a well formed number.

       - returns: A float parsed from the string or nil if it cannot be parsed.
    */
    func toFloat() -> Float? {

        let scanner = Scanner(string: self)
        var float: Float = 0

        if scanner.scanFloat(&float) {
            return float
        }

        return nil

    }

    func toJSON() -> JSON{
        do{
            let jsonencode :Data = self.data(using: String.Encoding.utf8)!
            let json = try JSONSerialization.jsonObject(with: jsonencode, options:[JSONSerialization.ReadingOptions.mutableContainers]) as! NSDictionary
            return JSON(json)
            
        }catch{
            return JSON("")
        }
        
    }
    /**
        Parses a string containing a non-negative integer value into an optional UInt if the string is a well formed number.

        - returns: A UInt parsed from the string or nil if it cannot be parsed.
    */
    func toUInt() -> UInt? {
        if let val = Int(self.trimmed()) {
            if val < 0 {
                return nil
            }
            return UInt(val)
        }

        return nil
    }
    
    func toInt() -> Int? {
        if let val = Int(self.trimmed()) {
            if val < 0 {
                return nil
            }
            return Int(val)
        }
        
        return nil
    }

    /**
      Parses a string containing a boolean value (true or false) into an optional Bool if the string is a well formed.

      - returns: A Bool parsed from the string or nil if it cannot be parsed as a boolean.
    */
    func toBool() -> Bool? {
        let text = self.trimmed().lowercased()
        if text == "true" || text == "false" || text == "yes" || text == "no" {
            return (text as NSString).boolValue
        }

        return nil
    }

    /**
      Parses a string containing a date into an optional NSDate if the string is a well formed.
      The default format is yyyy-MM-dd, but can be overriden.

      - returns: A NSDate parsed from the string or nil if it cannot be parsed as a date.
    */
    func toDate(_ format : String? = "yyyy-MM-dd") -> Date? {
        let text = self.trimmed().lowercased()
        let dateFmt = DateFormatter()
        dateFmt.timeZone = TimeZone.current
        if let fmt = format {
            dateFmt.dateFormat = fmt
        }
        return dateFmt.date(from: text)
    }

    /**
      Parses a string containing a date and time into an optional NSDate if the string is a well formed.
      The default format is yyyy-MM-dd hh-mm-ss, but can be overriden.

      - returns: A NSDate parsed from the string or nil if it cannot be parsed as a date.
    */
    func toDateTime(_ format : String? = "yyyy-MM-dd HH:mm:ss") -> Date? {
        return toDate(format)
    }

}
