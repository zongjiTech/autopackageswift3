//
//  Int.swift
//  guijiaoweike
//
//  Created by Lifefarmer on 2016/9/21.
//  Copyright © 2016年 danteng. All rights reserved.
//

import Foundation

public extension Int{
    func toTimeString(_ formate:String = "yyyy-MM-dd")->String{
        return Date(timeIntervalSince1970: Double(self)).toString(formate)
    }
    
    func toString(maxLenght num:UInt8)->String{
        return  NSString(format: "%.\(num)d" as NSString, self) as String
    }
}
