//
//  WebRouter.swift
//  AutoPackageipa
//
//  Created by Lifefarmer on 2016/10/12.
//  Copyright © 2016年 danteng. All rights reserved.
//

import Foundation
import Vapor

extension WebServer{
    
    func RegistRouter(){
        //根目录覆盖请求，用于验证服务是否启动
        drop.get("/") { request in
            return "Server Active"
        }
        //POST 请求测试
        drop.post("form") { request in
            let data = request.body.value.toJSON()
            return data.description
        }
    }
}
