//
//  NSColor+Wonderful.h
//  Wonderful
//
//  Created by dongshangxian on 15/10/26.
//  Copyright © 2015年 Sankuai. All rights reserved.
//

#import <Cocoa/Cocoa.h>


#define SXRGB16Color(rgbValue) [NSColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define SXRGBAColor(r,g,b,a) [NSColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]

#define Wonderful_RedColor1 [NSColor mistyRose]
#define Wonderful_RedColor2 [NSColor lightSalmon]
#define Wonderful_RedColor3 [NSColor lightCoral]
#define Wonderful_RedColor4 [NSColor salmonColor]
#define Wonderful_RedColor5 [NSColor coralColor]
#define Wonderful_RedColor6 [NSColor tomatoColor]
#define Wonderful_RedColor7 [NSColor orangeRed]
#define Wonderful_RedColor8 [NSColor indianRed]
#define Wonderful_RedColor9 [NSColor crimsonColor]
#define Wonderful_RedColor10 [NSColor fireBrick]

#define Wonderful_YelloeColor1 [NSColor cornColor]
#define Wonderful_YelloeColor2 [NSColor LemonChiffon]
#define Wonderful_YelloeColor3 [NSColor paleGodenrod]
#define Wonderful_YelloeColor4 [NSColor khakiColor]
#define Wonderful_YelloeColor5 [NSColor goldColor]
#define Wonderful_YelloeColor6 [NSColor orpimentColor]
#define Wonderful_YelloeColor7 [NSColor gambogeColor]
#define Wonderful_YelloeColor8 [NSColor realgarColor]
#define Wonderful_YelloeColor9 [NSColor goldenrod]
#define Wonderful_YelloeColor10 [NSColor darkGold]

#define Wonderful_GreenColor1 [NSColor paleGreen]
#define Wonderful_GreenColor2 [NSColor lightGreen]
#define Wonderful_GreenColor3 [NSColor springGreen]
#define Wonderful_GreenColor4 [NSColor greenYellow]
#define Wonderful_GreenColor5 [NSColor lawnGreen]
#define Wonderful_GreenColor6 [NSColor limeColor]
#define Wonderful_GreenColor7 [NSColor forestGreen]
#define Wonderful_GreenColor8 [NSColor seaGreen]
#define Wonderful_GreenColor9 [NSColor darkGreen]
#define Wonderful_GreenColor10 [NSColor olive]

#define Wonderful_CyanColor1 [NSColor lightCyan]
#define Wonderful_CyanColor2 [NSColor paleTurquoise]
#define Wonderful_CyanColor3 [NSColor aquamarine]
#define Wonderful_CyanColor4 [NSColor turquoise]
#define Wonderful_CyanColor5 [NSColor mediumTurquoise]
#define Wonderful_CyanColor6 [NSColor meituanColor]
#define Wonderful_CyanColor7 [NSColor lightSeaGreen]
#define Wonderful_CyanColor8 [NSColor darkCyan]
#define Wonderful_CyanColor9 [NSColor tealColor]
#define Wonderful_CyanColor10 [NSColor darkSlateGray]

#define Wonderful_BlueColor1 [NSColor skyBlue]
#define Wonderful_BlueColor2 [NSColor lightBLue]
#define Wonderful_BlueColor3 [NSColor deepSkyBlue]
#define Wonderful_BlueColor4 [NSColor doderBlue]
#define Wonderful_BlueColor5 [NSColor cornflowerBlue]
#define Wonderful_BlueColor6 [NSColor royalBlue]
#define Wonderful_BlueColor7 [NSColor mediumBlue]
#define Wonderful_BlueColor8 [NSColor darkBlue]
#define Wonderful_BlueColor9 [NSColor navyColor]
#define Wonderful_BlueColor10 [NSColor midnightBlue]

#define Wonderful_PurpleColor1 [NSColor lavender]
#define Wonderful_PurpleColor2 [NSColor thistleColor]
#define Wonderful_PurpleColor3 [NSColor plumColor]
#define Wonderful_PurpleColor4 [NSColor violetColor]
#define Wonderful_PurpleColor5 [NSColor mediumOrchid]
#define Wonderful_PurpleColor6 [NSColor darkOrchid]
#define Wonderful_PurpleColor7 [NSColor darkVoilet]
#define Wonderful_PurpleColor8 [NSColor blueViolet]
#define Wonderful_PurpleColor9 [NSColor darkMagenta]
#define Wonderful_PurpleColor10 [NSColor indigoColor]

#define Wonderful_GrayColor1 [NSColor whiteSmoke]
#define Wonderful_GrayColor2 [NSColor duckEgg]
#define Wonderful_GrayColor3 [NSColor gainsboroColor]
#define Wonderful_GrayColor4 [NSColor carapaceColor]
#define Wonderful_GrayColor5 [NSColor silverColor]
#define Wonderful_GrayColor6 [NSColor dimGray]

#define Wonderful_WhiteColor1 [NSColor seaShell]
#define Wonderful_WhiteColor2 [NSColor snowColor]
#define Wonderful_WhiteColor3 [NSColor linenColor]
#define Wonderful_WhiteColor4 [NSColor floralWhite]
#define Wonderful_WhiteColor5 [NSColor oldLace]
#define Wonderful_WhiteColor6 [NSColor ivoryColor]
#define Wonderful_WhiteColor7 [NSColor honeydew]
#define Wonderful_WhiteColor8 [NSColor mintCream]
#define Wonderful_WhiteColor9 [NSColor azureColor]
#define Wonderful_WhiteColor10 [NSColor aliceBlue]
#define Wonderful_WhiteColor11 [NSColor ghostWhite]
#define Wonderful_WhiteColor12 [NSColor lavenderBlush]
#define Wonderful_WhiteColor13 [NSColor beigeColor]

#define Wonderful_BrownColor1 [NSColor tanColor]
#define Wonderful_BrownColor2 [NSColor rosyBrown]
#define Wonderful_BrownColor3 [NSColor peruColor]
#define Wonderful_BrownColor4 [NSColor chocolateColor]
#define Wonderful_BrownColor5 [NSColor bronzeColor]
#define Wonderful_BrownColor6 [NSColor siennaColor]
#define Wonderful_BrownColor7 [NSColor saddleBrown]
#define Wonderful_BrownColor8 [NSColor soilColor]
#define Wonderful_BrownColor9 [NSColor maroonColor]
#define Wonderful_BrownColor10 [NSColor inkfishBrown]

#define Wonderful_PinkColor1 [NSColor waterPink]
#define Wonderful_PinkColor2 [NSColor lotusRoot]
#define Wonderful_PinkColor3 [NSColor lightPink]
#define Wonderful_PinkColor4 [NSColor mediumPink]
#define Wonderful_PinkColor5 [NSColor peachRed]
#define Wonderful_PinkColor6 [NSColor paleVioletRed]
#define Wonderful_PinkColor7 [NSColor deepPink]

@interface NSColor (Wonderful)

#pragma mark - **************** 红色系
/** 薄雾玫瑰*/
+ (NSColor *)mistyRose;
/** 浅鲑鱼色*/
+ (NSColor *)lightSalmon;
/** 淡珊瑚色*/
+ (NSColor *)lightCoral;
/** 鲑鱼色*/
+ (NSColor *)salmonColor;
/** 珊瑚色*/
+ (NSColor *)coralColor;
/** 番茄*/
+ (NSColor *)tomatoColor;
/** 橙红色*/
+ (NSColor *)orangeRed;
/** 印度红*/
+ (NSColor *)indianRed;
/** 猩红*/
+ (NSColor *)crimsonColor;
/** 耐火砖*/
+ (NSColor *)fireBrick;

#pragma mark - **************** 黄色系
/** 玉米色*/
+ (NSColor *)cornColor;
/** 柠檬薄纱*/
+ (NSColor *)LemonChiffon;
/** 苍金麒麟*/
+ (NSColor *)paleGodenrod;
/** 卡其色*/
+ (NSColor *)khakiColor;
/** 金色*/
+ (NSColor *)goldColor;
/** 雌黄*/
+ (NSColor *)orpimentColor;
/** 藤黄*/
+ (NSColor *)gambogeColor;
/** 雄黄*/
+ (NSColor *)realgarColor;
/** 金麒麟色*/
+ (NSColor *)goldenrod;
/** 乌金*/
+ (NSColor *)darkGold;

#pragma mark - **************** 绿色系
/** 苍绿*/
+ (NSColor *)paleGreen;
/** 淡绿色*/
+ (NSColor *)lightGreen;
/** 春绿*/
+ (NSColor *)springGreen;
/** 绿黄色*/
+ (NSColor *)greenYellow;
/** 草坪绿*/
+ (NSColor *)lawnGreen;
/** 酸橙绿*/
+ (NSColor *)limeColor;
/** 森林绿*/
+ (NSColor *)forestGreen;
/** 海洋绿*/
+ (NSColor *)seaGreen;
/** 深绿*/
+ (NSColor *)darkGreen;
/** 橄榄(墨绿)*/
+ (NSColor *)olive;

#pragma mark - **************** 青色系
/** 淡青色*/
+ (NSColor *)lightCyan;
/** 苍白绿松石*/
+ (NSColor *)paleTurquoise;
/** 绿碧*/
+ (NSColor *)aquamarine;
/** 绿松石*/
+ (NSColor *)turquoise;
/** 适中绿松石*/
+ (NSColor *)mediumTurquoise;
/** 美团色*/
+ (NSColor *)meituanColor;
/** 浅海洋绿*/
+ (NSColor *)lightSeaGreen;
/** 深青色*/
+ (NSColor *)darkCyan;
/** 水鸭色*/
+ (NSColor *)tealColor;
/** 深石板灰*/
+ (NSColor *)darkSlateGray;

#pragma mark - **************** 蓝色系
/** 天蓝色*/
+ (NSColor *)skyBlue;
/** 淡蓝*/
+ (NSColor *)lightBLue;
/** 深天蓝*/
+ (NSColor *)deepSkyBlue;
/** 道奇蓝*/
+ (NSColor *)doderBlue;
/** 矢车菊*/
+ (NSColor *)cornflowerBlue;
/** 皇家蓝*/
+ (NSColor *)royalBlue;
/** 适中的蓝色*/
+ (NSColor *)mediumBlue;
/** 深蓝*/
+ (NSColor *)darkBlue;
/** 海军蓝*/
+ (NSColor *)navyColor;
/** 午夜蓝*/
+ (NSColor *)midnightBlue;

#pragma mark - **************** 紫色系
/** 薰衣草*/
+ (NSColor *)lavender;
/** 蓟*/
+ (NSColor *)thistleColor;
/** 李子*/
+ (NSColor *)plumColor;
/** 紫罗兰*/
+ (NSColor *)violetColor;
/** 适中的兰花紫*/
+ (NSColor *)mediumOrchid;
/** 深兰花紫*/
+ (NSColor *)darkOrchid;
/** 深紫罗兰色*/
+ (NSColor *)darkVoilet;
/** 泛蓝紫罗兰*/
+ (NSColor *)blueViolet;
/** 深洋红色*/
+ (NSColor *)darkMagenta;
/** 靛青*/
+ (NSColor *)indigoColor;

#pragma mark - **************** 灰色系
/** 白烟*/
+ (NSColor *)whiteSmoke;
/** 鸭蛋*/
+ (NSColor *)duckEgg;
/** 亮灰*/
+ (NSColor *)gainsboroColor;
/** 蟹壳青*/
+ (NSColor *)carapaceColor;
/** 银白色*/
+ (NSColor *)silverColor;
/** 暗淡的灰色*/
+ (NSColor *)dimGray;

#pragma mark - **************** 白色系
/** 海贝壳*/
+ (NSColor *)seaShell;
/** 雪*/
+ (NSColor *)snowColor;
/** 亚麻色*/
+ (NSColor *)linenColor;
/** 花之白*/
+ (NSColor *)floralWhite;
/** 老饰带*/
+ (NSColor *)oldLace;
/** 象牙白*/
+ (NSColor *)ivoryColor;
/** 蜂蜜露*/
+ (NSColor *)honeydew;
/** 薄荷奶油*/
+ (NSColor *)mintCream;
/** 蔚蓝色*/
+ (NSColor *)azureColor;
/** 爱丽丝蓝*/
+ (NSColor *)aliceBlue;
/** 幽灵白*/
+ (NSColor *)ghostWhite;
/** 淡紫红*/
+ (NSColor *)lavenderBlush;
/** 米色*/
+ (NSColor *)beigeColor;

#pragma mark - **************** 棕色系
/** 黄褐色*/
+ (NSColor *)tanColor;
/** 玫瑰棕色*/
+ (NSColor *)rosyBrown;
/** 秘鲁*/
+ (NSColor *)peruColor;
/** 巧克力*/
+ (NSColor *)chocolateColor;
/** 古铜色*/
+ (NSColor *)bronzeColor;
/** 黄土赭色*/
+ (NSColor *)siennaColor;
/** 马鞍棕色*/
+ (NSColor *)saddleBrown;
/** 土棕*/
+ (NSColor *)soilColor;
/** 栗色*/
+ (NSColor *)maroonColor;
/** 乌贼墨棕*/
+ (NSColor *)inkfishBrown;

#pragma mark - **************** 粉色系
/** 水粉*/
+ (NSColor *)waterPink;
/** 藕色*/
+ (NSColor *)lotusRoot;
/** 浅粉红*/
+ (NSColor *)lightPink;
/** 适中的粉红*/
+ (NSColor *)mediumPink;
/** 桃红*/
+ (NSColor *)peachRed;
/** 苍白的紫罗兰红色*/
+ (NSColor *)paleVioletRed;
/** 深粉色*/
+ (NSColor *)deepPink;

@end
