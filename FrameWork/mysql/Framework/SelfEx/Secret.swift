//
//  secret.swift
//  whatEat
//
//  Created by farmerwu_pc on 15/7/2.
//  Copyright © 2015年 qiuqian. All rights reserved.
//

import Foundation
import CryptoSwift
class secret {
    
    //AES加密
    static func AesEncrypt(Key key:String, data:String)->String{
        
        //let jia = try! AES(key: key, iv: "", blockMode: .ecb)
        let jia = try! AES(key: key, iv: "", blockMode: .ECB, padding: PKCS7())
        let mydata = data.data(using: String.Encoding.utf8, allowLossyConversion: true)!
        let jiares = try! jia.encrypt(mydata.bytes)
        let resdata = Data(bytes: UnsafePointer<UInt8>(jiares), count: jiares.count)
        let jiastrres = resdata.base64EncodedString(options: Data.Base64EncodingOptions())
        return jiastrres
    }
    
    
    
    //AES解密
    static func AesDecrypt( _ key:String, data:String)->String{
        let jie = try!  AES(key: key, iv: "", blockMode: .ECB, padding: PKCS7())
        let nsdata = Data(base64Encoded: data)
        if let byte = nsdata?.bytes{
            do {
                let jieRes = try jie.decrypt(byte)
                let resdata = Data(bytes: jieRes)
                if let data = String(bytes: resdata.bytes, encoding: String.Encoding.utf8){
                    return data
                }else{
                    return ""
                }

            }catch{
                return ""
            }
        }else{
            return ""
        }
    }
}
