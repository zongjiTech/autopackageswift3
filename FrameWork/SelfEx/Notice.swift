//
//  Notice.swift
//  AutoPackageipa
//
//  Created by Lifefarmer on 2016/10/13.
//  Copyright © 2016年 danteng. All rights reserved.
//

import Cocoa

class Notice{
    
    static func waringMessage(message:String){
        let alert = NSAlert()
        alert.messageText = "出现问题"
        alert.informativeText = message
        alert.addButton(withTitle: "OK")
        alert.runModal()
    }
}
