//
//  NSColor+Wonderful.m
//  Wonderful
//
//  Created by dongshangxian on 15/10/26.
//  Copyright © 2015年 Sankuai. All rights reserved.
//

#import "NSColor+Wonderful.h"

@implementation NSColor (Wonderful)

#pragma mark - **************** 红色系
/** 薄雾玫瑰*/
+ (NSColor *)mistyRose
{
    return SXRGB16Color(0xFFE4E1);
}
/** 浅鲑鱼色*/
+ (NSColor *)lightSalmon
{
    return SXRGB16Color(0xFFA07A);
}
/** 淡珊瑚色*/
+ (NSColor *)lightCoral
{
    return SXRGB16Color(0xF08080);
}
/** 鲑鱼色*/
+ (NSColor *)salmonColor
{
    return SXRGB16Color(0xFA8072);
}
/** 珊瑚色*/
+ (NSColor *)coralColor
{
    return SXRGB16Color(0xFF7F50);
}
/** 番茄*/
+ (NSColor *)tomatoColor
{
    return SXRGB16Color(0xFF6347);
}
/** 橙红色*/
+ (NSColor *)orangeRed
{
    return SXRGB16Color(0xFF4500);
}
/** 印度红*/
+ (NSColor *)indianRed
{
    return SXRGB16Color(0xCD5C5C);
}
/** 猩红*/
+ (NSColor *)crimsonColor
{
    return SXRGB16Color(0xDC143C);
}
/** 耐火砖*/
+ (NSColor *)fireBrick
{
    return SXRGB16Color(0xB22222);
}


#pragma mark - **************** 黄色系
/** 玉米色*/
+ (NSColor *)cornColor
{
    return SXRGB16Color(0xFFF8DC);
}
/** 柠檬薄纱*/
+ (NSColor *)LemonChiffon
{
    return SXRGB16Color(0xFFFACD);
}
/** 苍金麒麟*/
+ (NSColor *)paleGodenrod
{
    return SXRGB16Color(0xEEE8AA);
}
/** 卡其色*/
+ (NSColor *)khakiColor
{
    return SXRGB16Color(0xF0E68C);
}
/** 金色*/
+ (NSColor *)goldColor
{
    return SXRGB16Color(0xFFD700);
}
/** 雌黄*/
+ (NSColor *)orpimentColor
{
    return SXRGB16Color(0xFFC64B);
}
/** 藤黄*/
+ (NSColor *)gambogeColor
{
    return SXRGB16Color(0xFFB61E);
}
/** 雄黄*/
+ (NSColor *)realgarColor
{
    return SXRGB16Color(0xE9BB1D);
}
/** 金麒麟色*/
+ (NSColor *)goldenrod
{
    return SXRGB16Color(0xDAA520);
}
/** 乌金*/
+ (NSColor *)darkGold
{
    return SXRGB16Color(0xA78E44);
}


#pragma mark - **************** 绿色系
/** 苍绿*/
+ (NSColor *)paleGreen
{
    return SXRGB16Color(0x98FB98);
}
/** 淡绿色*/
+ (NSColor *)lightGreen
{
    return SXRGB16Color(0x90EE90);
}
/** 春绿*/
+ (NSColor *)springGreen
{
    return SXRGB16Color(0x2AFD84);
}
/** 绿黄色*/
+ (NSColor *)greenYellow
{
    return SXRGB16Color(0xADFF2F);
}
/** 草坪绿*/
+ (NSColor *)lawnGreen
{
    return SXRGB16Color(0x7CFC00);
}
/** 酸橙绿*/
+ (NSColor *)limeColor
{
    return SXRGB16Color(0x00FF00);
}
/** 森林绿*/
+ (NSColor *)forestGreen
{
    return SXRGB16Color(0x228B22);
}
/** 海洋绿*/
+ (NSColor *)seaGreen
{
    return SXRGB16Color(0x2E8B57);
}
/** 深绿*/
+ (NSColor *)darkGreen
{
    return SXRGB16Color(0x006400);
}
/** 橄榄(墨绿)*/
+ (NSColor *)olive
{
    return SXRGB16Color(0x556B2F);
}


#pragma mark - **************** 青色系
/** 淡青色*/
+ (NSColor *)lightCyan
{
    return SXRGB16Color(0xE1FFFF);
}
/** 苍白绿松石*/
+ (NSColor *)paleTurquoise
{
    return SXRGB16Color(0xAFEEEE);
}
/** 绿碧*/
+ (NSColor *)aquamarine
{
    return SXRGB16Color(0x7FFFD4);
}
/** 绿松石*/
+ (NSColor *)turquoise
{
    return SXRGB16Color(0x40E0D0);
}
/** 适中绿松石*/
+ (NSColor *)mediumTurquoise
{
    return SXRGB16Color(0x48D1CC);
}
/** 美团色*/
+ (NSColor *)meituanColor
{
    return SXRGB16Color(0x2BB8AA);
}
/** 浅海洋绿*/
+ (NSColor *)lightSeaGreen
{
    return SXRGB16Color(0x20B2AA);
}
/** 深青色*/
+ (NSColor *)darkCyan
{
    return SXRGB16Color(0x008B8B);
}
/** 水鸭色*/
+ (NSColor *)tealColor
{
    return SXRGB16Color(0x008080);
}
/** 深石板灰*/
+ (NSColor *)darkSlateGray
{
    return SXRGB16Color(0x2F4F4F);
}


#pragma mark - **************** 蓝色系
/** 天蓝色*/
+ (NSColor *)skyBlue
{
    return SXRGB16Color(0xE1FFFF);
}
/** 淡蓝*/
+ (NSColor *)lightBLue
{
    return SXRGB16Color(0xADD8E6);
}
/** 深天蓝*/
+ (NSColor *)deepSkyBlue
{
    return SXRGB16Color(0x00BFFF);
}
/** 道奇蓝*/
+ (NSColor *)doderBlue
{
    return SXRGB16Color(0x1E90FF);
}
/** 矢车菊*/
+ (NSColor *)cornflowerBlue
{
    return SXRGB16Color(0x6495ED);
}
/** 皇家蓝*/
+ (NSColor *)royalBlue
{
    return SXRGB16Color(0x4169E1);
}
/** 适中的蓝色*/
+ (NSColor *)mediumBlue
{
    return SXRGB16Color(0x0000CD);
}
/** 深蓝*/
+ (NSColor *)darkBlue
{
    return SXRGB16Color(0x00008B);
}
/** 海军蓝*/
+ (NSColor *)navyColor
{
    return SXRGB16Color(0x000080);
}
/** 午夜蓝*/
+ (NSColor *)midnightBlue
{
    return SXRGB16Color(0x191970);
}


#pragma mark - **************** 紫色系
/** 薰衣草*/
+ (NSColor *)lavender
{
    return SXRGB16Color(0xE6E6FA);
}
/** 蓟*/
+ (NSColor *)thistleColor
{
    return SXRGB16Color(0xD8BFD8);
}
/** 李子*/
+ (NSColor *)plumColor
{
    return SXRGB16Color(0xDDA0DD);
}
/** 紫罗兰*/
+ (NSColor *)violetColor
{
    return SXRGB16Color(0xEE82EE);
}
/** 适中的兰花紫*/
+ (NSColor *)mediumOrchid
{
    return SXRGB16Color(0xBA55D3);
}
/** 深兰花紫*/
+ (NSColor *)darkOrchid
{
    return SXRGB16Color(0x9932CC);
}
/** 深紫罗兰色*/
+ (NSColor *)darkVoilet
{
    return SXRGB16Color(0x9400D3);
}
/** 泛蓝紫罗兰*/
+ (NSColor *)blueViolet
{
    return SXRGB16Color(0x8A2BE2);
}
/** 深洋红色*/
+ (NSColor *)darkMagenta
{
    return SXRGB16Color(0x8B008B);
}
/** 靛青*/
+ (NSColor *)indigoColor
{
    return SXRGB16Color(0x4B0082);
}


#pragma mark - **************** 灰色系
/** 白烟*/
+ (NSColor *)whiteSmoke
{
    return SXRGB16Color(0xF5F5F5);
}
/** 鸭蛋*/
+ (NSColor *)duckEgg
{
    return SXRGB16Color(0xE0EEE8);
}
/** 亮灰*/
+ (NSColor *)gainsboroColor
{
    return SXRGB16Color(0xDCDCDC);
}
/** 蟹壳青*/
+ (NSColor *)carapaceColor
{
    return SXRGB16Color(0xBBCDC5);
}
/** 银白色*/
+ (NSColor *)silverColor
{
    return SXRGB16Color(0xC0C0C0);
}
/** 暗淡的灰色*/
+ (NSColor *)dimGray
{
    return SXRGB16Color(0x696969);
}


#pragma mark - **************** 白色系
/** 海贝壳*/
+ (NSColor *)seaShell
{
    return SXRGB16Color(0xFFF5EE);
}
/** 雪*/
+ (NSColor *)snowColor
{
    return SXRGB16Color(0xFFFAFA);
}
/** 亚麻色*/
+ (NSColor *)linenColor
{
    return SXRGB16Color(0xFAF0E6);
}
/** 花之白*/
+ (NSColor *)floralWhite
{
    return SXRGB16Color(0xFFFAF0);
}
/** 老饰带*/
+ (NSColor *)oldLace
{
    return SXRGB16Color(0xFDF5E6);
}
/** 象牙白*/
+ (NSColor *)ivoryColor
{
    return SXRGB16Color(0xFFFFF0);
}
/** 蜂蜜露*/
+ (NSColor *)honeydew
{
    return SXRGB16Color(0xF0FFF0);
}
/** 薄荷奶油*/
+ (NSColor *)mintCream
{
    return SXRGB16Color(0xF5FFFA);
}
/** 蔚蓝色*/
+ (NSColor *)azureColor
{
    return SXRGB16Color(0xF0FFFF);
}
/** 爱丽丝蓝*/
+ (NSColor *)aliceBlue
{
    return SXRGB16Color(0xF0F8FF);
}
/** 幽灵白*/
+ (NSColor *)ghostWhite
{
    return SXRGB16Color(0xF8F8FF);
}
/** 淡紫红*/
+ (NSColor *)lavenderBlush
{
    return SXRGB16Color(0xFFF0F5);
}
/** 米色*/
+ (NSColor *)beigeColor
{
    return SXRGB16Color(0xF5F5DD);
}


#pragma mark - **************** 棕色系
/** 黄褐色*/
+ (NSColor *)tanColor
{
    return SXRGB16Color(0xD2B48C);
}
/** 玫瑰棕色*/
+ (NSColor *)rosyBrown
{
    return SXRGB16Color(0xBC8F8F);
}
/** 秘鲁*/
+ (NSColor *)peruColor
{
    return SXRGB16Color(0xCD853F);
}
/** 巧克力*/
+ (NSColor *)chocolateColor
{
    return SXRGB16Color(0xD2691E);
}
/** 古铜色*/
+ (NSColor *)bronzeColor // $$$$$
{
    return SXRGB16Color(0xB87333);
}
/** 黄土赭色*/
+ (NSColor *)siennaColor
{
    return SXRGB16Color(0xA0522D);
}
/** 马鞍棕色*/
+ (NSColor *)saddleBrown
{
    return SXRGB16Color(0x8B4513);
}
/** 土棕*/
+ (NSColor *)soilColor
{
    return SXRGB16Color(0x734A12);
}
/** 栗色*/
+ (NSColor *)maroonColor
{
    return SXRGB16Color(0x800000);
}
/** 乌贼墨棕*/
+ (NSColor *)inkfishBrown
{
    return SXRGB16Color(0x5E2612);
}


#pragma mark - **************** 粉色系
/** 水粉*/
+ (NSColor *)waterPink // $$$$$
{
    return SXRGB16Color(0xF3D3E7);
}
/** 藕色*/
+ (NSColor *)lotusRoot // $$$$$
{
    return SXRGB16Color(0xEDD1D8);
}
/** 浅粉红*/
+ (NSColor *)lightPink
{
    return SXRGB16Color(0xFFB6C1);
}
/** 适中的粉红*/
+ (NSColor *)mediumPink
{
    return SXRGB16Color(0xFFC0CB);
}
/** 桃红*/
+ (NSColor *)peachRed // $$$$$
{
    return SXRGB16Color(0xF47983);
}
/** 苍白的紫罗兰红色*/
+ (NSColor *)paleVioletRed
{
    return SXRGB16Color(0xDB7093);
}
/** 深粉色*/
+ (NSColor *)deepPink
{
    return SXRGB16Color(0xFF1493);
}


@end
